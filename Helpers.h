#pragma once
#include <string>
#include <iterator>
#include <vector>
#include <iostream>
#include <stdlib.h>

class timeout_exception : public std::runtime_error
{
public:
  using std::runtime_error::runtime_error;
};

inline std::vector<std::string> SplitHexString(const std::string& hexValue)
{
  // construct a stream from the string
  std::stringstream strstr(hexValue);

  // use stream iterators to copy the stream to the vector as whitespace separated strings
  std::istream_iterator<std::string> it(strstr);
  std::istream_iterator<std::string> end;
  std::vector<std::string> results(it, end);

  return results;
}

inline int ConvertHexToEngineRPM(std::vector<std::string> hexTokens)
{
  if (hexTokens.empty() || hexTokens.size() < 3) return 0;
  int valueA, valueB = 0;
  try
  {
    valueA = std::stoi(hexTokens[2], nullptr, 16);
    if (hexTokens.size() > 3)
    {
      valueB = std::stoi(hexTokens[3], nullptr, 16);
    }
  }
  catch (...)
  {
    return 0;
  }

  return (valueA * 256 + valueB) / 4;
}

inline int ConvertHexCoolantTemp(std::vector<std::string> hexTokens)
{
  if (hexTokens.empty() || hexTokens.size() < 3) return 0;
  int valueA;
  try
  {
    valueA = std::stoi(hexTokens[2], nullptr, 16);
  }
  catch (...)
  {
    return 0;
  }

  return valueA - 40;
}

inline int ConvertHexRoadSpeed(std::vector<std::string> hexTokens)
{
  if (hexTokens.empty() || hexTokens.size() < 3) return 0;
  int valueA;
  try
  {
    valueA = std::stoi(hexTokens[2], nullptr, 16);
  }
  catch (...)
  {
    return 0;
  }

  return valueA;
}

inline int ConvertHexThrottlePosition(std::vector<std::string> hexTokens)
{
  if (hexTokens.empty() || hexTokens.size() < 3) return 0;
  int valueA;
  try
  {
    valueA = std::stoi(hexTokens[2], nullptr, 16);
  }
  catch (...)
  {
    return 0;
  }

  return (valueA * 100) / 255;
}

inline void clrscr()
{
#if !defined(BOOST_WINDOWS) && !defined(__CYGWIN__)
  std::system("clear");
#else
  std::system("cls");
#endif
  return;
}