#include "ObdManager.h"
#include "Helpers.h"
#include <iostream>
#include <thread>
#include <chrono>
#include <boost\make_shared.hpp>

#define VALIDATE_OR_RETURN(condition, value) if(!condition) return value
constexpr auto ERROR_NULL_OR_EMPTY_PARAMETER = -2;
constexpr auto ERROR_INVALID_PORT = -1;
constexpr auto ERROR_INVALID_RESPONSE = "-3";

void ObdManager::SetPortName(std::string name)
{
  portName = name;
}

void ObdManager::Poll()
{
  std::string resultString = ERROR_INVALID_RESPONSE;

  // Send requests until a result is received
  resultString = GetDataByRequest(ENGINE_RPM_REQUEST_CODE, "1", 2);
  if (resultString != "-3")
  {
    EngineRpmHexString = resultString;
    resultString = ERROR_INVALID_RESPONSE;
  }

  resultString = GetDataByRequest(ENGINE_COOLANT_TEMP_REQUEST_CODE, "1", 1);
  if (resultString != "-3")
  {
    EngineCoolantTempHexString = resultString;
    resultString = ERROR_INVALID_RESPONSE;
  }

  resultString = GetDataByRequest(VEHICLE_ROAD_SPEED_REQUEST_CODE, "1", 1);
  if (resultString != "-3")
  {
    VehicleRoadSpeedHexString = resultString;
    resultString = ERROR_INVALID_RESPONSE;
  }

  resultString = GetDataByRequest(THROTTLE_POSITION_REQUEST_CODE, "1", 1);
  if (resultString != "-3")
  {
    ThrottlePositionHexString = resultString;
    resultString = ERROR_INVALID_RESPONSE;
  }
}

void ObdManager::PrintStoredFaultCodes()
{
  if (storedFaultCodes.size() == 0) return;

  std::cout << "Stored fault codes: \n";
  for (auto element : storedFaultCodes)
  {
    std::cout << element << std::endl;
  }
}

void ObdManager::ClosePort()
{
  FlushSerialPort(); // Don't need to throw in destructor

  if (port)
  {
    port->cancel();
    port->close();
    port.reset();
  }

  io.stop();
  io.reset();
}

int ObdManager::GetNumberOfFaultCodes()
{
  int byteCount;
  bool found = FALSE;
  const int bufferSize = 20;
  char pidBuffer[bufferSize];
  boost::system::error_code error;

  std::string sequence = "01 01\r";
  std::string response = "";

  while (!found)
  {
    byteCount = WriteToPort(sequence);
    byteCount = port->read_some(boost::asio::buffer(pidBuffer, bufferSize), error);

    std::string resultString(pidBuffer, sizeof(pidBuffer));
    auto indexOfPid = resultString.find("41 01");

    if (indexOfPid != std::string::npos)
    {
      found = TRUE;
      response = resultString;
    }
  }

  int numberOfFaultCodes = 0;
  std::vector<std::string> convertedResult = SplitHexString(response);
  if (convertedResult[2] == "81")
  {
    std::cout << "Number of codes too large or vehicle not operational." << std::endl;
    return 0;
  }
  else
  {
    numberOfFaultCodes = std::stoi(convertedResult[2]);
  }

  return numberOfFaultCodes;
}

void ObdManager::PrintFaultCodes(const int& numberOfFaultCodes)
{
  if (numberOfFaultCodes == 0) return;

  int byteCount;
  bool found = FALSE;
  const int bufferSize = 30;
  char pidBuffer[bufferSize];
  boost::system::error_code error;

  std::string sequence = "03\r";
  std::string response = "";

  while (!found)
  {
    byteCount = WriteToPort(sequence);
    byteCount = port->read_some(boost::asio::buffer(pidBuffer, bufferSize), error);

    std::string resultString(pidBuffer, sizeof(pidBuffer));
    auto indexOfPid = resultString.find("43");

    if (indexOfPid != std::string::npos)
    {
      found = TRUE;
      response = resultString;
    }
  }

  std::vector<std::string> convertedResult = SplitHexString(response);
  int counter = numberOfFaultCodes;

  std::cout << "Codes found:" << std::endl;
  for (auto i = 1; i < convertedResult.size() - 1; i++)
  {
    if (counter == 0) return;
    std::cout << convertedResult[i] << convertedResult[i + 1] << std::endl;
    storedFaultCodes.insert(convertedResult[i] + convertedResult[i + 1]);
    i = i + 1; // 2 in 2
    counter--;
  }
}

void ObdManager::InitializeSerialPort()
{
  port = boost::make_shared<boost::asio::serial_port>(io);
}

void ObdManager::OpenPort()
{
  // Try to open the connection
  boost::system::error_code errorCode;
  port->open(portName, errorCode);
  if (errorCode)
  {
    std::cout << "Cannot open port: " << portName << ", error code: " << errorCode.message().c_str() << std::endl;
    return;
  }

  // Set various params
  if (nullptr != port)
  {
    port->set_option(boost::asio::serial_port_base::baud_rate(baudRate));
    port->set_option(boost::asio::serial_port_base::character_size(8));
    port->set_option(boost::asio::serial_port_base::stop_bits(boost::asio::serial_port_base::stop_bits::one));
    port->set_option(boost::asio::serial_port_base::parity(boost::asio::serial_port_base::parity::none));
    port->set_option(boost::asio::serial_port_base::flow_control(boost::asio::serial_port_base::flow_control::none));

    // Discard stuff in the buffers
    const boost::system::error_code ec = FlushSerialPort();
    if (ec)
    {
      return;
    }

    // Reset the ELM device
    WriteToPort("ATZ\r"); // Reset command
    std::cout << "Resetting..." << std::endl;
    std::this_thread::sleep_for(std::chrono::seconds(2));
    ReadFromPort();

    
    WriteToPort("ATE0\r"); // Echo on
    std::cout << "Echo OFF" << std::endl;
    std::this_thread::sleep_for(std::chrono::seconds(1));
    ReadFromPort();
  }
  else
  {
    std::cout << "No connection to port. Exiting..." << std::endl;
    return;
  }
}

int ObdManager::WriteToPort(const std::string& sequence) const
{
  VALIDATE_OR_RETURN(!port.get() == NULL, ERROR_INVALID_PORT);
  VALIDATE_OR_RETURN(!sequence.empty(), ERROR_NULL_OR_EMPTY_PARAMETER);

  return port->write_some(boost::asio::buffer(sequence.c_str(), sequence.size()));
}

void ObdManager::FetchCurrentData()
{
  while (pollingWindowSize)
  {
    Poll();
    pollingWindowSize--;
  }

  pollingWindowSize = 50;
}

void ObdManager::FetchFaultCodes()
{
  int number = GetNumberOfFaultCodes();
  PrintFaultCodes(number);
}

std::string ObdManager::GetDataByRequest(std::string pid, std::string numberOfRequests, int numberOfBytes)
{
  const int bufferSize = 20;
  int count = 0;

  char pidBuffer[bufferSize];
  boost::system::error_code error;
  std::string returnedValue = "";

  bool isPollingActive = TRUE;
  while (isPollingActive)
  {
    try
    {
      // Receive
      count = WriteToPort(pid + " " + numberOfRequests + '\r');

      std::this_thread::sleep_for(std::chrono::milliseconds(100));
      port->read_some(boost::asio::buffer(pidBuffer, bufferSize), error);
    }
    catch (std::exception &e)
    {
      std::cout << e.what() << std::endl;
    }

    // Break cycle on return marker
    auto finder = std::find(std::begin(pidBuffer), std::end(pidBuffer), '>');
    if (finder != NULL)
    {
      isPollingActive = FALSE;
    }
  }

  std::string resultString(pidBuffer, sizeof(pidBuffer));

  // Change command header to RESPONSE
  pid.replace(0, 1, "4");

  auto indexOfPid = resultString.find(pid);
  auto noDataIndicator = resultString.find("DATA");
  if (indexOfPid != std::string::npos && noDataIndicator == std::string::npos && resultString.size() >= 7)
  {
    return resultString.substr(indexOfPid, 5 + 3 * numberOfBytes);
  }

  return ERROR_INVALID_RESPONSE;
}

// This Flush mechanism is not platform independent (right now it only works on Win) FIXED
boost::system::error_code ObdManager::FlushSerialPort()
{
  boost::system::error_code ec;
#if !defined(BOOST_WINDOWS) && !defined(__CYGWIN__)
  const bool isFlushed = !::tcflush(_serialPort.native(), TCIOFLUSH);
  if (!isFlushed)
  {
    ec = boost::system::error_code(errno, boost::asio::error::get_system_category());
  }
    
#else
  const bool isFlushed = ::PurgeComm(port->native_handle(), PURGE_RXABORT | PURGE_RXCLEAR | PURGE_TXABORT | PURGE_TXCLEAR);
  if (!isFlushed)
  {
    ec = boost::system::error_code(::GetLastError(), boost::asio::error::get_system_category());
  }
    
#endif 
  return ec;
}

int ObdManager::ReadFromPort() const
{
  const int bufferSize = 20;
  int count = 0;
  boost::system::error_code error;
  char pidBuffer[bufferSize];
  try
  {
    count = port->read_some(boost::asio::buffer(pidBuffer, bufferSize), error);
    std::cout << "READ BUFFER: " << pidBuffer << std::endl;
    return count;
  }
  catch (std::exception& e)
  {
    std::cerr << e.what() << std::endl;
  }

  return count;
}
