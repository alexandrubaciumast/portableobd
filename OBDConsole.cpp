// OBDCPPCONSOLE.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include "ObdManager.h"
#include "Helpers.h"
#include "Defines.h"

void MainMenu(ObdManager& manager);
void InfoMenu(ObdManager& manager);
int DecodeEngineRPMHex(std::string hexString);

int main()
{
  ObdManager man;
  MainMenu(man);

  return 0;
}

void MainMenu(ObdManager& manager) 
{
  while (true)
  {
    int opt;
    std::string newName;
    bool showValues = 1;

    std::cout << std::endl << "----------------------------";
    std::cout << std::endl << "      Portable OBD Reader   ";
    std::cout << std::endl << "----------------------------" << std::endl;
    std::cout << std::endl << "1. Set COM port name | Current port: " << manager.GetPortName() << std::endl;
    std::cout << std::endl << "2. Open port" << std::endl;
    std::cout << std::endl << "3. Show current values" << std::endl;
    std::cout << std::endl << "4. Show current fault codes" << std::endl;
    std::cout << std::endl << "5. Reference and Info" << std::endl;
    std::cout << std::endl << "0. Exit" << std::endl;
    std::cout << std::endl << "----------------------------" << std::endl;
    std::cin >> opt;
    std::cout << "\n" << std::endl;

    switch (opt)
    {
    case 1:
      std::cout << "Enter new name followed by RET: " << std::endl;
      std::cin >> newName;
      manager.SetPortName(newName);
      clrscr();
      break;
    case 2:
      manager.OpenPort();
      clrscr();
      break;
    case 3:
      // Check if port is open first
      if (!manager.IsPortOpen()) return;
      std::cout << "Fetching data. Please wait...";
      manager.FetchCurrentData();
      clrscr();

      std::cout << "Current vehicle parameters: \n" << std::endl;
      std::cout << "Engine RPM: " << ConvertHexToEngineRPM(SplitHexString(manager.EngineRpmHexString)) << std::endl;
      std::cout << "Engine Coolant Temp: " << ConvertHexCoolantTemp(SplitHexString(manager.EngineCoolantTempHexString)) << std::endl;
      std::cout << "Road Speed: " << ConvertHexRoadSpeed(SplitHexString(manager.VehicleRoadSpeedHexString)) << std::endl;
      std::cout << "Throttle Position: " << ConvertHexThrottlePosition(SplitHexString(manager.ThrottlePositionHexString)) << std::endl;

      std::cout << "\n\nPress any key to continue..."; std::cin.ignore(); std::cin.get();
      clrscr();
      break;
    case 4:
      if (!manager.IsPortOpen()) return;
      manager.FetchFaultCodes();
      std::cout << "\n\nPress any key to continue..."; std::cin.ignore(); std::cin.get();
      clrscr();
      break;
    case 5:
      clrscr();
      InfoMenu(manager);
      break;
    case 0:
      exit(1);
    default:
      break;
    }
  }
}

void InfoMenu(ObdManager& manager)
{
  bool exit = true;
  while (exit)
  {
    int opt;

    std::cout << std::endl << "----------------------------" << std::endl;
    std::cout << std::endl << "      Reference and Info   " << std::endl;
    std::cout << std::endl << "----------------------------" << std::endl;
    std::cout << std::endl << "1. Interpreting Trouble Codes" << std::endl;
    std::cout << std::endl << "2. Application Info" << std::endl;
    std::cout << std::endl << "0. Return" << std::endl;
    std::cout << std::endl << "----------------------------" << std::endl;
    std::cin >> opt;

    switch (opt)
    {
    case 1:
      // Decoding fault codes info
      manager.PrintStoredFaultCodes();
      std::cout << "Interpreting Trouble Codes\n\n";
      std::cout << "----------------------------------------" << std::endl;
      std::cout << "|First Digit|Decoded Chars|Code Section|" << std::endl;
      std::cout << "----------------------------------------" << std::endl;
      std::cout << "|   0 to 3  |   P0 to P3  | Power-train|" << std::endl;
      std::cout << "----------------------------------------" << std::endl;
      std::cout << "|   4 to 7  |   C0 to C3  |   Chassis  |" << std::endl;
      std::cout << "----------------------------------------" << std::endl;
      std::cout << "|   8 to B  |   B0 to B3  |    Body    |" << std::endl;
      std::cout << "----------------------------------------" << std::endl;
      std::cout << "|   C to F  |   U0 to U3  |   Network  |" << std::endl;
      std::cout << "----------------------------------------" << std::endl;
      std::cout << "The digits are always HEX (0 to F). Eg. A '01 03' code\nwill be decoded as P0103 (0 -> P0) + 1 03.\n This corresponds to the power-train code representing a\nMass or Volume Air flow Circuit High Input\n";
      std::cout << "\n\nPress any key to continue..."; std::cin.ignore(); std::cin.get();
      clrscr();
      break;
    case 2:
      std::cout << "Alexandru Baciu @ USV 2019" << std::endl;
      std::cout << "\n\nPress any key to continue..."; std::cin.ignore(); std::cin.get();
      clrscr();
      break;
    case 0:
      exit = false;
    }
  }
}
