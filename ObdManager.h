/// This class will be used to describe the
/// various functions that will be shown by the 
/// GUI. Later, we will bind the members to
/// GUI functions
#pragma once
#include <string>
#include <boost\asio.hpp>
#include <boost/bind.hpp>
#include <boost\array.hpp>
#include <set>

constexpr auto ENGINE_RPM_REQUEST_CODE = "01 0C";;
constexpr auto ENGINE_COOLANT_TEMP_REQUEST_CODE = "01 05";
constexpr auto VEHICLE_ROAD_SPEED_REQUEST_CODE = "01 0D";
constexpr auto THROTTLE_POSITION_REQUEST_CODE = "01 11";
constexpr auto ECU_DTC_REQUEST_CODE = "01 01";

typedef boost::shared_ptr<boost::asio::serial_port> serial_port_ptr;

#define MAX_BUFFER_SIZE 256;

class ObdManager
{
public:
  ObdManager()
  {
    InitializeSerialPort();
  }

  ~ObdManager()
  {
    ClosePort();
  }

  void InitializeSerialPort();
  void OpenPort();
  void Poll();
  int ReadFromPort() const;
  int WriteToPort(const std::string& sequence) const;
  void FetchCurrentData();
  void FetchFaultCodes();
  bool IsPortOpen() const { return (port != nullptr); }

  std::string GetDataByRequest(std::string pid, std::string numberOfRequests, int numberOfBytes);

  boost::system::error_code FlushSerialPort();
  void SetPortName(std::string name);
  std::string GetPortName() const { return portName; }

  std::string EngineRpmHexString;
  std::string EngineCoolantTempHexString;
  std::string VehicleRoadSpeedHexString;
  std::string ThrottlePositionHexString;
  void PrintStoredFaultCodes();
  int pollingWindowSize = 50;

private:
  void ClosePort();
  int GetNumberOfFaultCodes();
  void PrintFaultCodes(const int& numberOfFaultCodes);
  std::string portName = "COM6";
  const int baudRate = 9600;
  std::set<std::string> storedFaultCodes;

  // Serial port stuff
  boost::asio::io_service io;
  serial_port_ptr port;
};

